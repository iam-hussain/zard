import { motion, animate } from "framer-motion";
import React, { useEffect, useRef } from "react";

const Counter = ({ from, to, prefix, suffix, duration, ...arg }) => {
  const nodeRef = useRef();

  useEffect(() => {
    const node = nodeRef.current;

    const controls = animate(from, to, {
      duration: duration,
      onUpdate(value) {
        node.textContent = `${prefix ? prefix : ""}${value.toFixed()}${
          suffix ? suffix : ""
        }`;
      },
    });

    return () => controls.stop();
  }, [from, to, suffix, duration, prefix]);

  return <motion.span ref={nodeRef} {...arg} />;
};

export default Counter;
