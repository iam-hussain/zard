import React from "react";
import Head from "next/head";

// Import the ErrorBoundary component
import ErrorBoundary from "../components/ErrorBoundary";

import "../utils/style.css";

const App = ({ Component, pageProps }) => {
  return (
    <React.Fragment>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
        />
        <title>Jakir Hussain | Software Engineer | jHussain.com | Javascript Expert | Freelancer | Portfolio</title>
      </Head>
      <ErrorBoundary>
        <Component {...pageProps} />
      </ErrorBoundary>
    </React.Fragment>
  );
};

export default App;
