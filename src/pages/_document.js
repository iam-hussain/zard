import React from "react";
import Document, { Html, Head, Main, NextScript } from "next/document";

const metaData = {
  title: 'Jakir Hussain | Software Engineer | jHussain.com | Javascript Expert | Freelancer | Portfolio',
  description:
    "I am an enthusiastic person who wants to collaborate with an extraordinary team to learn more and upgrade my skills. I have been developing professionally for more than five years, but everything started in my school days with passion, self-learning, blogging, and freelancing.",
  site_name: "jHussain.com",
  creator: "Jakir Hussain",
  app_url: 'https://www.jhussain.com',
};
export default class MyDocument extends Document {

  render() {
    return (
      <Html lang="en">
        <Head>
        <meta name="description" content={metaData.description} />
          <meta property="og:type" content="website" />
          <meta name="og:title" property="og:title" content={metaData.title} />
          <meta name="og:description" property="og:description" content={metaData.description} />
          <meta property="og:site_name" content={metaData.site_name} />
          <meta property="og:url" content={metaData.app_url} />
          <meta name="twitter:card" content="summary" />
          <meta name="twitter:title" content={metaData.title} />
          <meta name="twitter:description" content={metaData.description} />
          <meta name="twitter:site" content={metaData.site_name} />
          <meta name="twitter:creator" content={metaData.creator} />
          <link rel="icon" href="/favicon.ico" />
          <link rel="apple-touch-icon" href="/favicon.ico" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
